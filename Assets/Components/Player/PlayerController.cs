﻿using UnityEngine;

public class PlayerController : MonoBehaviour, IKillable
{
    public float movementSpeed = 3.0f;
    public Vector2 playerSize = new Vector2(0.5f, 0.5f);

    private Vector3 _deltaMove;

    private void Update()
    {
        Move();
        RestrictMovement();
    }

    private void Move()
    {
        _deltaMove = Input.GetAxisRaw("Horizontal") * Vector3.right;
        _deltaMove += Input.GetAxisRaw("Vertical") * Vector3.up;
        _deltaMove.Normalize();
        transform.position += _deltaMove * movementSpeed * Time.deltaTime;
    }

    private void RestrictMovement()
    {
        var pos0 = playerSize.x * Input.GetAxisRaw("Horizontal") * Vector3.right;
        pos0 += playerSize.y * Input.GetAxisRaw("Vertical") * Vector3.up;

        var pos1 = transform.position + pos0;

        var pos2 = Camera.main.WorldToViewportPoint(pos1);
        pos2.x = Mathf.Clamp(pos2.x, 0f, 1f);
        pos2.y = Mathf.Clamp(pos2.y, 0f, 1f);

        var newPos = Camera.main.ViewportToWorldPoint(pos2);
        transform.position = newPos - pos0;
    }

    private void OnDrawGizmosSelected()
    {
        var pos0 = playerSize.x * Input.GetAxisRaw("Horizontal") * Vector3.right;
        pos0 += playerSize.y * Input.GetAxisRaw("Vertical") * Vector3.up;
        var pos1 = transform.position + pos0;

        Gizmos.color = Color.red;
        Gizmos.DrawSphere(pos1, 0.1f);
    }

    public void Kill()
    {
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        Debug.Log("GAME OVER");
        Time.timeScale = 0;
    }
}