﻿using UnityEngine;

public class DoppelgangerBehaviour : SpawnObjectBehaviour, IKillable
{
    private bool _isEngaged;
    private static Transform _player;

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (_player == null)
            {
                gameObject.layer = LayerMask.NameToLayer("Player");
                _player = other.transform;
                _isEngaged = true;
                Time.timeScale = 0.5f;
            }
            else
            {
                Kill();
            }
        }
        else if (other.gameObject.CompareTag("Doppelganger"))
        {
            Kill();
        }
    }

    protected override void Update()
    {
        if (!_isEngaged)
        {
            base.Update();
        }
        else
        {
            var pos = _player.position;
            transform.position = new Vector3(-pos.x, pos.y, pos.z);
        }
    }

    public void Kill()
    {
        if (_isEngaged)
        {
            _player = null;
            Time.timeScale = 1f;
        }

        Destroy(gameObject);
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}