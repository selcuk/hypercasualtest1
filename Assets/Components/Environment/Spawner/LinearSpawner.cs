﻿using System.Collections;
using UnityEngine;

public class LinearSpawner : MonoBehaviour
{
    public Vector3 range;
    public SpawnObjectProperties[] spawnObjects;

    private void Start()
    {
        foreach (var spawnObject in spawnObjects)
        {
            if (spawnObject.spawnPrefab != null)
            {
                StartCoroutine(SpawnCounter(spawnObject));
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        var halfRange = range / 2;
        Gizmos.DrawLine(transform.position - halfRange, transform.position + halfRange);
    }

    private IEnumerator SpawnCounter(SpawnObjectProperties spawnObjectProperties)
    {
        while (spawnObjectProperties.isActive)
        {
            SpawnObstacle(spawnObjectProperties);
            yield return new WaitForSeconds(spawnObjectProperties.interval);
        }
    }

    private Vector3 GetSpawnPosition()
    {
        var halfRange = range / 2;
        var x = Random.Range(-halfRange.x, halfRange.x);
        var y = Random.Range(-halfRange.y, halfRange.y);
        var z = Random.Range(-halfRange.z, halfRange.z);
        var relativePos = new Vector3(x, y, z);
        var newPos = transform.position + relativePos;
        return newPos;
    }

    private void SpawnObstacle(SpawnObjectProperties spawnObjectProperties)
    {
        var pos = GetSpawnPosition();

        var instance = Instantiate(spawnObjectProperties.spawnPrefab, pos, Quaternion.identity);
        var SpawnBehaviour = instance.GetComponent<SpawnObjectBehaviour>();
        if (SpawnBehaviour)
        {
            SpawnBehaviour.Velocity = spawnObjectProperties.direction.normalized * spawnObjectProperties.movementSpeed;
        }
    }
}