﻿using UnityEngine;

public class SpawnObjectBehaviour : MonoBehaviour
{
    public Vector3 Velocity { get; set; }
    protected Rigidbody Rigidbody;

    protected void Start()
    {
        Rigidbody = GetComponent<Rigidbody>();
    }

    protected virtual void Update()
    {
        if (Rigidbody != null)
        {
            Rigidbody.velocity = Velocity;
        }
    }
}