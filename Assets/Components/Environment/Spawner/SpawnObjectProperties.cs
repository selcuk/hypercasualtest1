using System;
using UnityEngine;

[Serializable]
public struct SpawnObjectProperties
{
    public GameObject spawnPrefab;
    public Vector3 direction;
    public float movementSpeed;
    public float interval;
    public bool isActive;
}