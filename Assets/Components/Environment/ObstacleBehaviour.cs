﻿using UnityEngine;

public class ObstacleBehaviour : SpawnObjectBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        var killable = other.gameObject.GetComponent<IKillable>();
        if (killable != null)
        {
            killable.Kill();
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}